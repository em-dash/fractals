/* make a png image with a fractal on it  */

/* pixels are arranged in memory rgbrgbrgbrgb and then rows of pixels are
 * fucking wherever apparently (in an array of row pointers) */

/* TODO argument parsing with unistd.h */
/* TODO clean up and organise files */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <zlib.h>
#include <png.h>

#define BIT_DEPTH 8

struct complex_double {
    double r;
    double i;
};

enum fractal_type {
    MANDELBROT,
    JULIA
};

/* takes each of HSV as whole numbers between 0 and 255 */
/* pixel[0] is r, pixel[1] is g, pixel[2] is b */
/* this is the slow algorithm according to wikipedia but i couldn't be bothered
 * with the fast one */
void color_set_hsv(
        unsigned char * pixel,
        unsigned char hue,
        unsigned char saturation,
        unsigned char value) {
    double chroma;
    /* chroma has range of 0..255 */

    chroma = ((double) value) * ((double) saturation) / 255.0;

    /* there's probably a cool maths theory on how to do selection tests like 
     * this most efficiently maybe look it up some time. */
    /* but honestly i'm probably being a dweeb and it should just be a
     * switch */
    /* X = C x (1 - | H' mod 2 - 1 |) */
    /* we are using the hue paramater which is 0..255 instead of H' which is 
     * 0..6 so it's mod 85.0 instead of mod 2 */
    if (hue <= 128) {
          if (hue <= 43) { 
            /* C X 0 */
            pixel[0] = value;
            pixel[1] = (char) (chroma * (1.0 - fabs(fmod(((double) hue / 42.5),
                2.0) - 1.0)) + value - chroma);
            pixel[2] = value - (char) chroma;
            return;
        }      
        if (hue <= 85) {
            /* X C 0 */
            pixel[0] = (char) (chroma * (1.0 - fabs(fmod(((double) hue / 42.5),
                2.0) - 1.0)) + value - chroma);
            pixel[1] = value;
            pixel[2] = value - (char) chroma;
            return;
        }
        /* 0 C X */
        pixel[0] = value - (char) chroma;
        pixel[1] = value;
        pixel[2] = (char) (chroma * (1.0 - fabs(fmod(((double) hue / 42.5),
            2.0) - 1.0)) + value - chroma);
        return;
    }
    if (hue <= 213) {
        if (hue <= 170) {
            /* 0 X C */
            pixel[0] = value - (char) chroma;
            pixel[1] = (char) (chroma * (1.0 - fabs(fmod(((double) hue / 42.5),
                2.0) - 1.0)) + value - chroma);
            pixel[2] = value;
            return;
        }
        /* X 0 C */
        pixel[0] = (char) (chroma * (1.0 - fabs(fmod(((double) hue / 42.5),
            2.0) - 1.0)) + value - chroma);
        pixel[1] = value - (char) chroma;
        pixel[2] = value;
        return;
    }
    if (hue <= 255) {
        /* C 0 X */
        pixel[0] = value;
        pixel[1] = value - (char) chroma;
        pixel[2] = (char) (chroma * (1.0 - fabs(fmod(((double) hue / 42.5),
            2.0) - 1.0)) + value - chroma);
        return;
    }
    /* TODO if we reach here in the code something has gone very wrong */
}

struct complex_double complex_double_square(const struct complex_double x) {
    struct complex_double result;

    result.r = x.r * x.r - x.i * x.i;
    result.i = 2.0 * x.r * x.i;
    return result;
}

void print_help() {
    /* TODO write the help lol */
    printf("help goes here");
}

int main(int argc, char * argv[]) {
    /* pixels is the actual array of all the pixels. 
     * row_pointers is because libpng. */
    unsigned char * pixels;
    unsigned char ** row_pointers;
    unsigned int image_width = 1024;
    unsigned int image_height = 1024;
    unsigned int iterations = 100;
    double x_scale = 3.0;
    double y_scale = 3.0;
    double cparam_r = 0.0;
    double cparam_i = 0.0;
    FILE * fp;
    char filename[] = "image.png";
    png_structp png_ptr;
    png_infop info_ptr;
    enum fractal_type type = MANDELBROT;
    int opt;
    struct complex_double z;
    struct complex_double c;
    struct complex_double * iterated_value;
    struct complex_double * constant_value;
    size_t i, j, k;


    /* TODO add fractal type option 't' */
    while ((opt = getopt(argc, argv, "hx:y:n:r:i:")) != -1) {
        switch (opt) {
            case 'h': /* help */
                print_help();
                return 0;
            case 'x': /* width (x size) */
                image_width = atoi(optarg);
                if (image_width < 1) {
                    printf("Invalid width, must be an integer greater than " 
                            "0\n");
                    return 1;
                }
                break;
            case 'y': /* height (y size) */
                image_height = atoi(optarg);
                if (image_height < 1) {
                    printf("Invalid height, must be an integer greater than "
                            "0\n");
                    return 1;
                }
                break;
            case 'n':
                iterations = atoi(optarg);
                if (iterations < 1) {
                    printf("Invalid iterations, must be an integer greater "
                    "than 0\n");
                    return 1;
                }
                break;
            case 'r':
                cparam_r = atof(optarg);
                break;
            case 'i':
                cparam_i = atof(optarg);
                break;
            default:
                break;

        }
    }


    /* TODO actually check if the malloc() succeeds you dweeb */
    pixels = malloc(sizeof (unsigned char) * image_height * image_width * 3);

    switch (type) {
        case MANDELBROT: /* mandelbrot */
            iterated_value = &c;
            constant_value = &z;
            break;
        case JULIA: /* julia */
            iterated_value = &z;
            constant_value = &c;
            break;
    }

    /* process the image scale. the x_scale and the y_scale determine the range
     * that the image will display. the user can set either, but not both; the
     * other value will be set here. */
    

    for (i = 0; i < image_height; i++) {
        /* TODO add params for scaling/moving image */
        for (j = 0; j < image_width; j++) {
            iterated_value->i = ((double) i - (double) image_height / 2.0) /
                (double) image_height * 3.0; 
            iterated_value->r = ((double) image_width / 2.0 - (double) j) /
                (double) image_width * 3.0;
            constant_value->r = cparam_r;
            constant_value->i = cparam_i;
            for (k = 0; k < iterations; k++) {
                z = complex_double_square(z);
                z.r += c.r;
                z.i += c.i;
                if (sqrt(z.r * z.r + z.i * z.i) > ((double) 4)) {
                    break;
                }
            }
            /* TODO make these colors scale more nicely (independent of
             * iterations parameter) and add paramaters for them */
            /* TODO maybe make the hues scale nonlinearly (polynomically 
             * (that's definitely not a word))*/
            if (k == iterations) {
                color_set_hsv(&(pixels[(i * image_width + j) * 3]), 0, 0, 0);
            } else {
                /* i guess technically this can be done inside the for(k = ...)
                 * loop? but we maybe still need an if statement 
                 * of break, */
                color_set_hsv(&(pixels[(i * image_width + j) * 3]), 
                              (char) ((k * 20) % 255 - 80), 
                              127, 
                              k * 100 / iterations + 150);
            }
        }
    }

    /* make a special array of shit for libpng */
    row_pointers = malloc(sizeof (unsigned char *) * image_height);
    for (i = 0; i < image_height; i++) {
        row_pointers[i] = &pixels[i * image_width * 3];
    }


    /* libpng init */
    fp = fopen(filename, "wb");
    if (!fp)
        return 1;
    png_ptr = png_create_write_struct(
        PNG_LIBPNG_VER_STRING,
        (png_voidp) NULL,
        NULL,
        NULL);
    if (!png_ptr)
        return 1;
    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) {
        png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
        return 1;
    }

    png_init_io(png_ptr, fp);
    png_set_IHDR(
        png_ptr,
        info_ptr,
        image_width,
        image_height,
        BIT_DEPTH,
        PNG_COLOR_TYPE_RGB,
        PNG_INTERLACE_NONE,
        PNG_COMPRESSION_TYPE_DEFAULT,
        PNG_FILTER_TYPE_DEFAULT);
    png_write_info(png_ptr, info_ptr);

    /* write array of pixels to png file */
    png_write_image(png_ptr, (png_bytepp) row_pointers);

    /* clean up */
    png_write_end(png_ptr, info_ptr);

    if (setjmp (png_jmpbuf(png_ptr))) {
        png_destroy_write_struct(&png_ptr, &info_ptr);
        fclose(fp);
        return 1;
    }

    free(pixels);

    return 0;
}
