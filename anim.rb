#!/usr/bin/ruby

theta = 0.0
i = 0
processes = 0

while theta < (3.141592653589793 * 2)
  started = Process.fork do
    puts "starting frame #{i} at #{(Math.cos(theta) * 0.1) + 0.3} + #{(Math.sin(theta)) * 0.05 + 0.4}i"
    `/home/meredith/src/fractals/frac 2000 2000 50 1\
    	#{(Math.cos(theta) * 0.1) + 0.3} #{(Math.sin(theta) * 0.05) + 0.4} out/#{i.to_s.rjust(8, '0')}.png`
  end
  if started
    theta += 0.005
    i += 1
    processes += 1
  else
    puts "can't make a process :("
  end
    
  
  if processes >= 8
    Process.wait
    processes -= 1 
end
  
end
